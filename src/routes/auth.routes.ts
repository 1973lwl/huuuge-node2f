import { Router } from "express";
import jwt from 'jsonwebtoken'
import assert from 'assert'
import { mockUsersList } from "./mockUsersList";

interface LoginPayload {
  username: string;
}

export const authRoutes = Router()

  .post<{}, {}, LoginPayload>('/login', (req, res) => {

    assert(process.env.JWT_SECRET, 'JWT_SECRET not set')

    const access_token = jwt.sign(mockUsersList[0], process.env.JWT_SECRET, {
      algorithm: 'HS256',
      // expiresIn,
    });

    res.send({
      type:'Bearer',
      expiresIn:'0',
      access_token
    })
  })

import * as express from 'express';
import { UserDTOs, UserListError, UserListResponse, UserQueryParams } from '../interfaces/user.interfaces';

import joi from 'joi'
import { type } from 'os';
import fs from 'fs';
import path from 'path';
import { EventEmitter } from 'events';
import { emit } from 'process';
import { pipeline } from 'stream';
import zlib from 'zlib'
import { mockUsersList } from './mockUsersList';

export const userRoutes = express.Router()
// .use(middleware)

const querySchema = joi.object({
  filter: joi.string().required(),
  limit: joi.string().default(10),
  offset: joi.string().default(0),
})

userRoutes.get<{}, UserListResponse | UserListError, null, UserQueryParams>('/', (req, res) => {
  let { filter/* , limit = '10', offset = '0' */ } = req.query
  const limit = parseInt(req.query.limit || '10')
  const offset = parseInt(req.query.offset || '10')

  const { error, value } = querySchema.validate(req.query)

  if (error) {
    return res.status(400).send({ error: error.message })
  }

  const userResponse = {
    items: mockUsersList,
    filter,
    limit,
    offset,
  };

  res.send(userResponse)
})


userRoutes.get<null, UserDTOs.User, null, null>('/me', (req, res) => {
  res.send({
    id: '123', name: '123', active: true
  })
})

userRoutes.get<{ id: string }, UserDTOs.User>("/:id", function (req, res) {
  res.send({
    id: '123', name: '123', active: true
  });
});

userRoutes.get('/:id/image', (req, res) => {

  const imagePath = path.join(__dirname, '../../data/users/', req.params.id, '/avatar.jpg')
  console.log(imagePath)

  res.download(imagePath)
})

/* Fule Upload */
userRoutes.get('/avatar', (req, res) => {
  res.sendFile('../../data')
})

userRoutes.post('/avatar/upload', (req, res) => {
  res.set('Content-Type','text/html')
  res.flushHeaders()
  res.write('<pre>' + JSON.stringify(req.headers,null,2))
  req.pipe(res)
})


type CreateUserDTO = Omit<UserDTOs.User, 'id' | 'active'> & { password: string }


const createUserSchema = joi.object({
  name: joi.string().required(),
  password: joi.string().required()
})

userRoutes.post<{}, UserDTOs.User | UserListError, CreateUserDTO>('/', (req, res) => {
  const { error, value } = createUserSchema.validate(req.body)

  if (error) {
    return res.status(400).send({ error: error.message })
  }

  const createdUser = {
    id: Date.now().toString(),
    active: false,
    ...value
  };
  mockUsersList.push(createdUser)

  // Notify other parts of application
  // appEventBuss.emit('user:created',createdUser)
  // appEventBuss.emit(eventsEvent.user.created,createdUser)

  const { password, ...userWithoutPassword } = createdUser
  res.status(201).send(userWithoutPassword)
})

// type userKeys = 'id' | 'name' | 'profile'
// type UserSansId = {
// [key in userKeys]: string
// [key in keyof CreateUserDTO]?: CreateUserDTO[key]
// readonly [key in keyof CreateUserDTO]: CreateUserDTO[key]
// }

// type Partial<T> = {
//   [key in keyof T]?: T[key]
// }

// type UserSansId = Pick<UserDTOs.User,'id'>
type UserSansId = Omit<UserDTOs.User, 'id'>

// const obj: UserSansId = {
// name
// }

// userRoutes.post('/register', (req, res) => {

// })


userRoutes.get('/calculate', (req, res) => {

  const date = Date.now() + 10_000
  // while (Date.now() < date) { }

  const handle = setInterval(() => {
    console.log('working hard...')

    if (Date.now() > date) {
      clearInterval(handle)
      res.end('<h1>Bye bye! </h1>')
    } else {
      res.flushHeaders()
      res.write('<h1>Hello you </h1>')
    }
  }, 0)

})


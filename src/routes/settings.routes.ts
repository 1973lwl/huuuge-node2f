import { Router } from 'express';
import {ISetting} from '../interfaces/settings.interfaces'

export const settingsRoutes = Router();




settingsRoutes.get('/test', (req, res) => {
  res.send({
    settings: [
      {
        settingId: 1,
        settingName: "Opcja ingnorowana przez system",
        settingValue:
        {
          values:
          [{
            isNewest: true,
            setValue: "Who cares",
          changedBy: "Hakier",
          changedDate: Date.now().toString()
          },
          {
            isNewest: false,
            setValue: "nobody cares",
            changedBy: "author",
            changedDate: '10-10-10'//Date.toString(Date.now() - 100000)
          }]
        },
      }]
  })
  })
    

  settingsRoutes.get('/:id', (req, res) => {
  
    const response: ISetting =
    {
      id : req.params['id'],
      name : "fake name for id=" + req.params['id'],
      value : "fake value for id=" + req.params['id']
    }
    res.send(response)
  })
  

  settingsRoutes.get('/', (req, res) => {
    res.send("Nic, absolutnie nic")
  })
  
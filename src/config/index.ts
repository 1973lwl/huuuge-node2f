
import express, { Express, NextFunction, RequestHandler } from 'express'
import { configErrorHandlers } from './errorhandlers'
import cors from 'cors'
import morgan from 'morgan'
import helmet from 'helmet'
import timeout from 'connect-timeout'
import responseTime from 'response-time'
import tracer from 'express-request-id'
import jwt from 'express-jwt'
import assert from 'assert'

/* === Config === */
export const configMiddlewares = (app: Express) => {
  app.use(responseTime())
  app.use(tracer())
  app.use(cors({}))
  // app.use(helmet({}))
  app.use(morgan('dev', {}))

  assert(process.env.JWT_SECRET, 'JWT_SECRET not set')
  app.use(
    jwt({
      secret: process.env.JWT_SECRET, algorithms: ['HS256'],
      credentialsRequired: false
    })
  )

  if (process.env.NODE_ENV == 'production') {
    app.use(timeout('5s'))
  }

  // app.use(express.urlencoded())
  app.use(express.json({}))


  app.use((req,res,next)=>{
    req.placki
    next()
  })
}

export default configMiddlewares


declare global {
  namespace Express {

    interface Request {
      /**
       * Most important part of Request
       */
      placki: string
    }
  }
}
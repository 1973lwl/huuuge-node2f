
import express, { Express, NextFunction, RequestHandler } from 'express'
import errorhandler from 'errorhandler'
import cors from 'cors'

export const configErrorHandlers = (app: Express) => {
  
  if (process.env.NODE_ENV === 'development') {
    // only use in development
    app.use(errorhandler())
  } else {
    app.use((err: any, req: express.Request, res: express.Response, next: NextFunction) => {
      res.send({ error: 'Unexpected Error' })
      // next(err)
    })
  }
}
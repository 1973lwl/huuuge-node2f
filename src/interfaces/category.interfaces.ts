
export interface CategoryQueryParams {
  id?: string,
  name?: string
}

export interface CategoryListResponse {
  name?: string,
  level?: number
}
